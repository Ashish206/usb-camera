/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.jiangdg.usbcamera;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.jiangdg.usbcamera";
  public static final String BUILD_TYPE = "debug";
  public static final int VERSION_CODE = 99;
  public static final String VERSION_NAME = "1.2.1.20200414";
}
