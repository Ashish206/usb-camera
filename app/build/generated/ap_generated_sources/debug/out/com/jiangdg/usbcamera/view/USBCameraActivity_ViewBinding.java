// Generated code from Butter Knife. Do not modify!
package com.jiangdg.usbcamera.view;

import android.view.View;
import android.widget.SeekBar;
import android.widget.Switch;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.appcompat.widget.Toolbar;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.jiangdg.usbcamera.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class USBCameraActivity_ViewBinding implements Unbinder {
  private USBCameraActivity target;

  @UiThread
  public USBCameraActivity_ViewBinding(USBCameraActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public USBCameraActivity_ViewBinding(USBCameraActivity target, View source) {
    this.target = target;

    target.mTextureView = Utils.findRequiredView(source, R.id.camera_view, "field 'mTextureView'");
    target.mToolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'mToolbar'", Toolbar.class);
    target.mSeekBrightness = Utils.findRequiredViewAsType(source, R.id.seekbar_brightness, "field 'mSeekBrightness'", SeekBar.class);
    target.mSeekContrast = Utils.findRequiredViewAsType(source, R.id.seekbar_contrast, "field 'mSeekContrast'", SeekBar.class);
    target.mSwitchVoice = Utils.findRequiredViewAsType(source, R.id.switch_rec_voice, "field 'mSwitchVoice'", Switch.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    USBCameraActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mTextureView = null;
    target.mToolbar = null;
    target.mSeekBrightness = null;
    target.mSeekContrast = null;
    target.mSwitchVoice = null;
  }
}
